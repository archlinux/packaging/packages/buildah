# Maintainer: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Maintainer: Morten Linderud <foxboron@archlinux.org>
# Maintainer: George Rawlinson <grawlinson@archlinux.org>

pkgname=buildah
pkgver=1.39.2
pkgrel=1
pkgdesc="A tool which facilitates building OCI images"
arch=(x86_64)
url="https://github.com/containers/buildah"
license=(Apache-2.0)
depends=(oci-runtime skopeo passt libseccomp gpgme)
makedepends=(go git bats btrfs-progs device-mapper gpgme libassuan
             bzip2 go-md2man skopeo systemd)
options=(!lto !debug)
source=(git+https://github.com/containers/buildah.git#tag=v$pkgver)
sha512sums=('8d362fe812c23ad7f04c8287eb6bc57c326e8fefe7e7642651530f09b9da447ee46e7d8570b8dfbc0c1e9293a46405b41e1af3364c61eb9bc1324107f7ad7ab3')

build() {
  cd $pkgname
  export GOFLAGS="-buildmode=pie -trimpath"
  export CGO_LDFLAGS="${LDFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  make PREFIX=/usr BUILDTAGS="containers_image_ostree_stub seccomp"
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" PREFIX=usr install install.completions
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
